reqs
====

 - https://gitlab.com/Solace-10/libveilid-core
    

build
=====
 - build libveilid-core the ffi-aiding c layer
   - `cd ../libveilid-core && cmake . && make && cd ../veilid-go`
 - `mkdir lib`
 - `cp ../libveilid-core/libveilid_core_ffi.a lib`
 - `cp ../libveilid-core/libveilid_core_ffi.so lib`
 - `cp ../libveilid-core/include/veilidcore.h lib`
 - `go build -ldflags="-r $(pwd)/lib"`


ref
===

 - https://docs.rs/veilid-core/0.2.1/veilid_core/
 - https://github.com/mediremi/rust-plus-golang
