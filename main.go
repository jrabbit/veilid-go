package main
/*
#cgo LDFLAGS: -L./lib -lveilid_core_ffi -ldl
#include "./lib/veilidcore.h"
*/
import "C"
import "fmt"


func main() {
	 fmt.Println(C.veilid_version())
	 fmt.Printf("%+v", C.veilid_version())
}
